#Base OS image 
FROM openjdk:8-alpine

# Add Maintainer Info
LABEL maintainer="mail2prasanthv@gmail.com"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=build/libs/SampleProject-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} /usr/app/SampleProject.jar

#Set current working directory
WORKDIR /usr/app

# Run the jar file 
ENTRYPOINT ["java","-jar","/SampleProject.jar]
