package com.docker.example;

import java.util.Date;

import lombok.Data;

@Data
public class Employee {
	private int id;
	private String name;
	private int age;
	private int rollNumber;
	private String address;
	private Date dob;
}
