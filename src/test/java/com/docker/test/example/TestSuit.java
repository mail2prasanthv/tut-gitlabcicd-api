package com.docker.test.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class TestSuit {
    @Test
    public void testFindByName() {
        assertEquals("Foo", "Foo");
    }
}